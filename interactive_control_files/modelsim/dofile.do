# Renames sim dataset to vsim
set wave_type view_only
if {[lsearch -exact [dataset list] sim] >= 0} {
    dataset rename sim vsim
    set wave_type sim
}

# simulation controls
add wave -divider "simulation controls"
add wave -label initdone vsim/:tb_acorechip:initdone
add wave -label simdone vsim/:tb_acorechip:simdone

# clock and reset
add wave -divider "clock and reset"
add wave -label clock -color "yellow" vsim/:tb_acorechip:clock
add wave -label reset -color "yellow" vsim/:tb_acorechip:reset

radix define PcSrc {
    3'h0 "PC_4"   -color green
    3'h1 "RELATIVE"  -color green 
    3'h2 "ABSOLUTE"   -color green 
    3'h3 "TRAP"   -color green 
    3'h4 "PC"   -color green 
    3'h5 "MEPC"   -color green 
    3'h6 "PC_2"   -color green 
}


radix define mcause {
    32'h00 "INSTR_ADDR_MISALIG"   -color green
    32'h01 "INSTR_ADDR_FAULT"  -color green 
    32'h02 "ILLEGAL_INSTR"   -color green 
    32'h03 "BREAKPOINT"   -color green 
    32'h04 "LOAD_ADDR_MISALIG"   -color green 
    32'h05 "LOAD_ACCESS_FAULT"   -color green 
    32'h06 "STORE_ADDR_MISALIG"   -color green 
    32'h07 "STORE_ADDR_FAULT"   -color green 
}

# trap
add wave -divider "Trap signals"
add wave -label Trap_flag -color "Green" vsim/:tb_acorechip:acorechip:core:trap_ctrl:io_out_trap
add wave -label mcause -color "green" vsim/:tb_acorechip:acorechip:core:trap_ctrl:io_out_mcause
radix signal vsim/:tb_acorechip:acorechip:core:trap_ctrl:io_out_mcause mcause

# pipeline
add wave -divider "Pipeline signals"
add wave -label branch -color "Yellow" vsim/:tb_acorechip:acorechip:core:alu_block_io_out_data_branch_taken
add wave -label jump -color "Yellow" vsim/:tb_acorechip:acorechip:core:ppl_ctrl_io_in_jump
add wave -label jump_extended_out -color "Yellow" vsim/:tb_acorechip:acorechip:core:ppl_ctrl:jump_flush_delay_counter_extended_out

# IFA-stage
add wave -divider "IFA-stage"
add wave -label valid -color "Orange" vsim/:tb_acorechip:acorechip:core:ifa_valid
add wave -label flush -color "Orange" vsim/:tb_acorechip:acorechip:core:ppl_ctrl_io_out_flush_ifa_ifd

# IFD-stage
add wave -divider "IFD-stage"
add wave -label reg_pc -color "Yellow" vsim/:tb_acorechip:acorechip:core:ifa_ifd_reg_pc
radix signal vsim/:tb_acorechip:acorechip:core:ifa_ifd_reg_pc hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:ifa_ifd_reg_pc hexadecimal
add wave -label ifetch_rdata -color "Yellow" vsim/:tb_acorechip:acorechip:core:io_ifetch_rdata
radix signal vsim/:tb_acorechip:acorechip:core:io_ifetch_rdata hexadecimal 
add wave -label valid -color "Yellow" vsim/:tb_acorechip:acorechip:core:ifd_valid
add wave -label ready -color "Yellow" vsim/:tb_acorechip:acorechip:core:ifd_ready
add wave -label flush -color "Yellow" vsim/:tb_acorechip:acorechip:core:ppl_ctrl_io_out_flush_ifd_id

# ID-stage
add wave -divider "ID-stage"
add wave -label reg_pc -color "Blue" vsim/:tb_acorechip:acorechip:core:ifd_id_reg_pc
radix signal vsim/:tb_acorechip:acorechip:core:ifd_id_reg_pc hexadecimal
add wave -label reg_ifetch_rdata -color "Blue" vsim/:tb_acorechip:acorechip:core:ifd_id_reg_ifetch_rdata
radix signal vsim/:tb_acorechip:acorechip:core:ifd_id_reg_ifetch_rdata hexadecimal
add wave -label instruction_text vsim/:tb_acorechip:io_debug_instruction_text
add wave -label imm -color "orange" vsim/:tb_acorechip:acorechip:core:decoder_block:io_imm
radix signal vsim/:tb_acorechip:io_debug_instruction_text ascii
radix signal vsim/:tb_acorechip:acorechip:core:decoder_block:io_imm hexadecimal
add wave -label valid -color "Blue" vsim/:tb_acorechip:acorechip:core:id_valid
add wave -label ready -color "Blue" vsim/:tb_acorechip:acorechip:core:id_ready
add wave -label stall -color "Blue" vsim/:tb_acorechip:acorechip:core:ppl_ctrl_io_out_stall_id_ex
add wave -label reg_rd -color "Blue" vsim/:tb_acorechip:acorechip:core:id_ex_reg_rd
radix signal vsim/:tb_acorechip:acorechip:core:id_ex_reg_rd hexadecimal
add wave -label reg_rs1 -color "Blue" vsim/:tb_acorechip:acorechip:core:id_ex_reg_rs1
radix signal vsim/:tb_acorechip:acorechip:core:id_ex_reg_rs1 hexadecimal
add wave -label reg_rs1_rdata -color "Blue" vsim/:tb_acorechip:acorechip:core:id_ex_reg_rs1_rdata
radix signal vsim/:tb_acorechip:acorechip:core:id_ex_reg_rs1_rdata hexadecimal
add wave -label reg_rs2_rdata -color "Blue" vsim/:tb_acorechip:acorechip:core:id_ex_reg_rs2_rdata
radix signal vsim/:tb_acorechip:acorechip:core:id_ex_reg_rs2_rdata hexadecimal

# EX-stage
add wave -divider "EX-stage"
add wave -label reg_pc -color "Yellow" vsim/:tb_acorechip:acorechip:core:id_ex_reg_pc
radix signal vsim/:tb_acorechip:acorechip:core:id_ex_reg_pc hexadecimal
add wave -label valid -color "Yellow" vsim/:tb_acorechip:acorechip:core:ex_valid
add wave -label ready -color "Yellow" vsim/:tb_acorechip:acorechip:core:ex_ready
add wave -label reg_rd -color "Yellow" vsim/:tb_acorechip:acorechip:core:id_ex_reg_rd
add wave -label reg_rs1 -color "Yellow" vsim/:tb_acorechip:acorechip:core:id_ex_reg_rs1
add wave -label reg_rs1_rdata -color "Yellow" vsim/:tb_acorechip:acorechip:core:id_ex_reg_rs1_rdata
add wave -label reg_rs2_rdata -color "Yellow" vsim/:tb_acorechip:acorechip:core:id_ex_reg_rs2_rdata
add wave -label branch_taken -color "Yellow" vsim/:tb_acorechip:acorechip:core:alu_block_io_out_data_branch_taken
add wave -label alu_result -color "Yellow" vsim/:tb_acorechip:acorechip:core:alu_block_io_out_data_result
radix signal vsim/:tb_acorechip:acorechip:core:alu_block_io_out_data_result hexadecimal

# MEMA-stage
add wave -divider "MEMA-stage"
add wave -label reg_pc -color "Orange" vsim/:tb_acorechip:acorechip:core:ex_mema_reg_pc
radix signal vsim/:tb_acorechip:acorechip:core:ex_mema_reg_pc hexadecimal
add wave -label valid -color "Orange" vsim/:tb_acorechip:acorechip:core:mema_valid
add wave -label ready -color "Orange" vsim/:tb_acorechip:acorechip:core:mema_ready

# MEMD-stage
add wave -divider "MEMD-stage"
add wave -label reg_pc -color "Orange" vsim/:tb_acorechip:acorechip:core:mema_memd_reg_pc
radix signal vsim/:tb_acorechip:acorechip:core:mema_memd_reg_pc hexadecimal
add wave -label valid -color "Orange" vsim/:tb_acorechip:acorechip:core:memd_valid

# WB-stage
add wave -divider "WB-stage"

# programming interface
# add wave -divider "programming interface"
# add wave -label prog_write_en vsim/:tb_acorechip:io_prog_write_en
# add wave -label programming_iface_addr vsim/:tb_acorechip:io_programming_iface_addr
# add wave -label programming_iface_data vsim/:tb_acorechip:io_programming_iface_data

# jtag tap
add wave -divider "jtag tap"
add wave -label TCK vsim/:tb_acorechip:io_jtag_TCK
add wave -label TMS vsim/:tb_acorechip:io_jtag_TMS
add wave -label TDI vsim/:tb_acorechip:io_jtag_TDI
add wave -label TRSTn vsim/:tb_acorechip:io_jtag_TRSTn
add wave -label TDO_data vsim/:tb_acorechip:io_jtag_TDO_data
add wave -label TDO_driven vsim/:tb_acorechip:io_jtag_TDO_driven

# Test Data Registers
add wave -divider "TDR"
add wave -label 1_addr -color "purple" vsim/:tb_acorechip:acorechip:jtag:tdrio_out_1
add wave -label 2_data -color "purple" vsim/:tb_acorechip:acorechip:jtag:tdrio_out_2 
add wave -label 3_wrt_en -color "purple" vsim/:tb_acorechip:acorechip:jtag:tdrio_out_3 

# other control and status signals
add wave -divider "other control and status signals"
add wave -label core_en vsim/:tb_acorechip:acorechip:core:io_core_en
add wave -label core_fault vsim/:tb_acorechip:io_core_fault

# GPIO
add wave -divider "GPIO"
add wave -label "gpio out" vsim/:tb_acorechip:acorechip:io_gpo
add wave -label "gpio in" vsim/:tb_acorechip:acorechip:io_gpi
radix signal vsim/:tb_acorechip:acorechip:io_gpi hexadecimal
radix signal vsim/:tb_acorechip:acorechip:io_gpo hexadecimal

# instruction fetch
add wave -divider "instruction fetch"
add wave -label ifetch_req vsim/:tb_acorechip:acorechip:core:io_ifetch_req
add wave -label ifetch_gnt vsim/:tb_acorechip:acorechip:core:io_ifetch_gnt
add wave -label ifetch_raddr vsim/:tb_acorechip:acorechip:core:io_ifetch_raddr
add wave -label ifetch_rdata vsim/:tb_acorechip:acorechip:core:io_ifetch_rdata
add wave -label ifetch_rdata_valid vsim/:tb_acorechip:acorechip:core:io_ifetch_rdata_valid
radix signal vsim/:tb_acorechip:acorechip:core:io_ifetch_raddr hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:io_ifetch_rdata hexadecimal


# regfile
add wave -divider "register file"
add wave -label x1/ra -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_1 
add wave -label x2/sp -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_2 
add wave -label x3/gp -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_3 
add wave -label x4/tp -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_4 
add wave -label x5/t0 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_5 
add wave -label x6/t1 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_6 
add wave -label x7/t2 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_7 
add wave -label x8/s0 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_8 
add wave -label x9/s1 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_9 
add wave -label x10/a0 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_10 
add wave -label x11/a1 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_11 
add wave -label x12/a2 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_12 
add wave -label x13/a3 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_13 
add wave -label x14/a4 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_14 
add wave -label x15/a5 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_15 
add wave -label x16/a6 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_16 
add wave -label x17/a7 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_17 
add wave -label x18/s2 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_18 
add wave -label x19/s3 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_19 
add wave -label x20/s4 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_20 
add wave -label x21/s5 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_21 
add wave -label x22/s6 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_22 
add wave -label x23/s7 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_23 
add wave -label x24/s8 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_24 
add wave -label x25/s9 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_25 
add wave -label x26/s10 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_26 
add wave -label x27/s11 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_27 
add wave -label x28/t3 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_28 
add wave -label x29/t4 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_29 
add wave -label x30/t5 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_30 
add wave -label x31/t6 -color "royal blue" vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_31

radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_1  hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_2  hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_3  hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_4  hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_5  hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_6  hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_7  hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_8  hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_9  hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_10 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_11 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_12 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_13 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_14 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_15 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_16 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_17 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_18 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_19 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_20 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_21 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_22 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_23 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_24 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_25 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_26 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_27 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_28 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_29 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_30 hexadecimal
radix signal vsim/:tb_acorechip:acorechip:core:regfile_block:regfile:x_31 hexadecimal

radix define MemOpWidth {
    2'b00 "BYTE"   -color green
    2'b01 "HWORD"  -color yellow 
    2'b10 "WORD"   -color orange 
    2'b11 "DWORD"  -color red
}

# memory interface
add wave -divider "memory interface"
add wave -label req         -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_req
add wave -label gnt         -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_gnt
add wave -label ren         -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_info_ren
add wave -label wen         -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_info_wen
add wave -label op_width    -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_info_op_width
radix signal vsim/:tb_acorechip:acorechip:lsu:io_core_info_op_width MemOpWidth
add wave -label op_unsigned -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_info_op_unsigned
add wave -label addr        -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_info_addr
add wave -label wdata       -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_info_wdata
add wave -label rdata_valid -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_rdata_valid
add wave -label rdata       -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_rdata_bits
add wave -label fault       -color "aqua" vsim/:tb_acorechip:acorechip:lsu:io_core_fault

# AXI4-Lite signaling
add wave -divider "AXI Read Address"
add wave -label ARREADY -color "coral" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_AR_ready
add wave -label ARVALID -color "coral" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_AR_valid
add wave -label ARADDR  -color "coral" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_AR_bits_ADDR
add wave -label ARPROT  -color "coral" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_AR_bits_PROT
radix signal vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_AR_bits_ADDR hexadecimal

add wave -divider "AXI Read Data"
add wave -label RREADY -color "coral" vsim/:tb_acorechip:acorechip:lsu:io_lsu_port_R_ready
add wave -label RVALID -color "coral" vsim/:tb_acorechip:acorechip:lsu:io_lsu_port_R_valid
add wave -label RDATA  -color "coral" vsim/:tb_acorechip:acorechip:lsu:io_lsu_port_R_bits_DATA
add wave -label RRESP  -color "coral" vsim/:tb_acorechip:acorechip:lsu:io_lsu_port_R_bits_RESP
radix signal vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_R_bits_DATA hexadecimal

add wave -divider "AXI Write Address"
add wave -label AWREADY -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_AW_ready
add wave -label AWVALID -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_AW_valid
add wave -label AWADDR  -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_AW_bits_ADDR
add wave -label AWPROT  -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_AW_bits_PROT
radix signal vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_AW_bits_ADDR hexadecimal

add wave -divider "AXI Write Data"
add wave -label WREADY -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_W_ready
add wave -label WVALID -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_W_valid
add wave -label WDATA  -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_W_bits_DATA
add wave -label WSTRB  -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_W_bits_STRB
radix signal vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_W_bits_DATA hexadecimal

add wave -divider "AXI Write Response"
add wave -label BREADY -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_B_ready
add wave -label BVALID -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_B_valid
add wave -label BRESP  -color "orchid" vsim/:tb_acorechip:acorechip:lsu:lsu:io_lsu_port_B_bits_RESP

#radix signal vsim/:tb_acorechip:io_prog_write_en binary
#radix signal vsim/:tb_acorechip:io_programming_iface_addr hexadecimal
#radix signal vsim/:tb_acorechip:io_programming_iface_data hexadecimal

radix signal vsim/:tb_acorechip:acorechip:jtag:tdrio_out_1 unsigned
radix signal vsim/:tb_acorechip:acorechip:jtag:tdrio_out_2 unsigned
radix signal vsim/:tb_acorechip:acorechip:jtag:tdrio_out_3 unsigned

radix signal vsim/:tb_acorechip:acorechip:lsu:lsu:io_core_info_addr     hexadecimal
radix signal vsim/:tb_acorechip:acorechip:lsu:lsu:io_core_info_wdata    hexadecimal
radix signal vsim/:tb_acorechip:acorechip:lsu:lsu:io_core_rdata_bits        hexadecimal

if { $wave_type == {sim} } {
    onfinish stop
    run -all
}

view wave
wave zoom full
