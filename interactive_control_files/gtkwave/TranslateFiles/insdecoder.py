#!/usr/bin/env python3

# GTKWave Translate Process File to decode hex value into RISC-V operation
# Credits go to Matt Venn
# Source: https://github.com/mattvenn/gtkwave-python-filter-process (2022)

import sys
import tempfile
import subprocess


def main():
    fh_in = sys.stdin
    fh_out = sys.stdout
    instr_dict = {}

    while True:
        try:
            l = fh_in.readline()
            if not l:
                return 0

            if "x" in l:
                fh_out.write(l)
                fh_out.flush()
                continue
            
            if str(l) not in instr_dict.keys():
                obj_temp = tempfile.NamedTemporaryFile(delete=False, mode='w')
                with tempfile.NamedTemporaryFile(delete=False, mode='w') as asm_temp:
                    # Convert l from string to int
                    l_int = int(l, 16)
                    # If compressed instruction, shift the relevant bits to MSB
                    # so that objdump interprets them correctly
                    if (l_int & 3) != 3:
                        l_int = (l_int & 0xFFFF) << 16
                    asm_temp.write(".word %d\n" % l_int)
                    asm_temp.flush()
                    subprocess.run(["riscv64-unknown-elf-as", "-march=rv32imfc", "-o", obj_temp.name, asm_temp.name])
                    result = subprocess.run(["riscv64-unknown-elf-objdump", "-Dj", ".text",  obj_temp.name], stdout=subprocess.PIPE)
                    lastline = result.stdout.splitlines()[-1]
                    chunks = lastline.decode().split('\t')

                    opcodes = " ".join(chunks[2:])
                    instr_dict[str(l)] = opcodes
                
            fh_out.write("%s\n" % instr_dict[str(l)])
            fh_out.flush()
        except Exception as e:
            print(e)


if __name__ == '__main__':
	sys.exit(main())
