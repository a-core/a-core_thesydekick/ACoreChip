# SPDX-License-Identifier: Apache-2.0

# Delete previous signals on display
gtkwave::/Edit/Highlight_All
gtkwave::/Edit/Delete

# Global signals
set sig_idx 0
set alias_idx 1
set radix_idx 2
set tfile_idx 3
set tfile_path /TranslateFiles/
variable tcl_path [file normalize [info script]]

#### Procedure definitions ####

# Returns path of the tcl script
proc getResourceDirectory {} {
  variable tcl_path
  return [file dirname $tcl_path]
}

# Reloads this Tcl
proc reloadTcl {} {
  global tcl_path
  gtkwave::/File/Read_Tcl_Script_File $tcl_path
}

# Adds a signal list
# Positional arguments: signal list, color, name for comment trace
proc add_signal_list { sig_list color {topic ""} } {
  global sig_idx
  global alias_idx
  global radix_idx
  global tfile_idx
  global tfile_path
  upvar $sig_list x
  # Add a comment trace if there is one
  if {[string length $topic] != 0} {
    gtkwave::addCommentTracesFromList [list "$topic"]
  }
  foreach signal $x {
    set temp_list [ list ]
    lappend temp_list [lindex $signal $sig_idx]
    gtkwave::addSignalsFromList $temp_list
    gtkwave::highlightSignalsFromList $temp_list
    gtkwave::/Edit/Color_Format/$color

    array set nargs {-alias "undef" -radix "undef" -tfile "undef" -tproc "undef"}
    array set nargs [lrange $signal 1 end]

    set alias $nargs(-alias)
    set radix $nargs(-radix)
    set tfile $nargs(-tfile)
    set tproc $nargs(-tproc)

    ## Rename the signal if an alias was provided
    if { $alias != "undef" } {
      gtkwave::/Edit/Alias_Highlighted_Trace $alias
      # Needs a re-highlight as the signal name changes
      gtkwave::highlightSignalsFromList [list $alias]
    }

    ## Set data format
    if { $radix != "undef" } {
      gtkwave::/Edit/Data_Format/$radix
    }

    ## Set translation filter file
    if { $tfile != "undef" } {
      set which_f [ gtkwave::setCurrentTranslateFile [getResourceDirectory]$tfile_path$tfile]
      gtkwave::installFileFilter $which_f
    }

    ## Set translation process file
    if { $tproc != "undef" } {
      set which_f [ gtkwave::setCurrentTranslateProc [getResourceDirectory]$tfile_path$tproc]
      gtkwave::installProcFilter $which_f
    }
  }
}

#### Signals (READ FIRST) ####
#
# Signal bundles are defined here
# Note that they must still be added to gtkwave with 'add_signal_list' command (see end of file)
#
# Signals are added by first creating a list, then appending signals to that list with lappend.
# A simple signal contains only the name: lappend siglist {"signalname"}
# In addition, you can provide named arguments:
# -alias: Rename the signal in the waveform viewer
# -radix: set the radix (number format: Hex, Decimal, ASCII etc.)
# -tfile: Translate File name (these should be under TranslateFiles/, provide only file name without path)
# -tproc: Translate Process File name (same rules apply as to tfile)
# Example: lappend siglist {"signalname" -alias signame -tfile sigtfile.txt}
#
# Note! When providing a translate (process) file, you should provide a radix that matches with the radix
# used in the translate (process) file
# Note! Translate Process Files must be set as executable (e.g. with chmod +x tprocfile.py)

set sys_signals [ list \
  {"ACoreChip.core_clock" -alias clock} \
  {"ACoreChip.core_reset" -alias reset} \
]

set pipeline [ list \
  {"ACoreChip.core.ifd_empty" -alias ifd_empty} \
  {"ACoreChip.core.ppl_ctrl_io_in_branch" -alias branch(gend_at_EX)} \
  {"ACoreChip.core.ppl_ctrl_io_in_jump" -alias jump(gend_at_ID)} \
  {"ACoreChip.core.ppl_ctrl.jump_flush_delay_counter_extended_out" -alias jump_extended_out} \
]

set jtag [ list \
  {"ACoreChip.io_jtag_TCK" -alias TCK } \
  {"ACoreChip.io_jtag_TMS" -alias TMS } \
  {"ACoreChip.io_jtag_TDI" -alias TDI } \
  {"ACoreChip.io_jtag_TRSTn" -alias TRSTn } \
  {"ACoreChip.io_jtag_TDO_data" -alias TDO_data } \
  {"ACoreChip.io_jtag_TDO_driven" -alias TDO_driven } \
]

set PC [list]
lappend PC {"ACoreChip.core.pc_block.pcSelect.io_out_pc_src" -radix Decimal -tfile PcSrc.txt}
lappend PC {"ACoreChip.core.pc_block.io_out_pc"}

set Trap [list]
lappend Trap {"ACoreChip.core.trap_ctrl.io_out_trap" -alias trap}
lappend Trap {"ACoreChip.core.trap_ctrl.io_out_mcause" -alias mcause -radix Decimal -tfile mcause.txt}


set IFA [list]
lappend IFA {"ACoreChip.core.ifa_ifd_next_pc"}
lappend IFA {"ACoreChip.core.ifa_valid"}
lappend IFA {"ACoreChip.core.ifa_ready"}
lappend IFA {"ACoreChip.core.ppl_ctrl_io_out_flush_ifa_ifd" -alias flush_ifa_ifd}

set IFD [list]
lappend IFD {"ACoreChip.core.ifa_ifd_reg_pc"}
lappend IFD {"ACoreChip.core.io_ifetch_rdata"}
lappend IFD {"ACoreChip.core.ifd_valid"}
lappend IFD {"ACoreChip.core.ifd_ready"}
lappend IFD {"ACoreChip.core.ppl_ctrl_io_out_flush_ifd_id" -alias flush_ifd_id}

set ID [list]
lappend ID {"ACoreChip.core.ifd_id_reg_pc"}
lappend ID {"ACoreChip.core.ifd_id_reg_ifetch_rdata" -alias Instruction -radix Hex -tproc insdecoder.py}
lappend ID {"ACoreChip.core.ifd_id_reg_ifetch_rdata"}
lappend ID {"ACoreChip.core.id_valid"}
lappend ID {"ACoreChip.core.id_ready"}
lappend ID {"ACoreChip.core.stall"}
lappend ID {"ACoreChip.core.id_ex_next_rd"}
lappend ID {"ACoreChip.core.id_ex_next_rs1"}
lappend ID {"ACoreChip.core.id_ex_next_rs1_rdata"}
lappend ID {"ACoreChip.core.id_ex_next_rs2_rdata"}

set EX [list]
lappend EX {"ACoreChip.core.id_ex_reg_pc"}
lappend EX {"ACoreChip.core.ex_valid"}
lappend EX {"ACoreChip.core.ex_ready"}
lappend EX {"ACoreChip.core.id_ex_reg_rd"}
lappend EX {"ACoreChip.core.id_ex_reg_rs1"}
lappend EX {"ACoreChip.core.id_ex_reg_rs1_rdata"}
lappend EX {"ACoreChip.core.id_ex_reg_rs2_rdata"}
lappend EX {"ACoreChip.core.alu_block_io_out_data_branch_taken"}
lappend EX {"ACoreChip.core.alu_block_io_out_data_result"}

set MEMA [list]
lappend MEMA {"ACoreChip.core.ex_mema_reg_pc"}
lappend MEMA {"ACoreChip.core.mema_valid"}
lappend MEMA {"ACoreChip.core.mema_ready"}
lappend MEMA {"ACoreChip.core.ex_mema_reg_rd"}
lappend MEMA {"ACoreChip.core.ex_mema_reg_rs1"}
lappend MEMA {"ACoreChip.core.ex_mema_reg_rs1_rdata"}
lappend MEMA {"ACoreChip.core.ex_mema_reg_rs2_rdata"}

set MEMD [list]
lappend MEMD {"ACoreChip.core.mema_memd_reg_pc"}
lappend MEMD {"ACoreChip.core.memd_valid"}
lappend MEMD {"ACoreChip.core.memd_ready"}

set WB [list]
lappend WB {"ACoreChip.core.memd_wb_reg_pc"}
lappend WB {"ACoreChip.core.wb_valid"}
lappend WB {"ACoreChip.core.wb_ready"}

# If you add signals like this, you can comment out unwanted signals
set regfile_int [ list ]
# lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_0"  -alias x0/zero }
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_1"  -alias x1/ra   } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_2"  -alias x2/sp   } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_3"  -alias x3/gp   } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_4"  -alias x4/tp   } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_5"  -alias x5/t0   } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_6"  -alias x6/t1   } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_7"  -alias x7/t2   } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_8"  -alias x8/s0/fp} 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_9"  -alias x9/s1   } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_10" -alias x10/a0  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_11" -alias x11/a1  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_12" -alias x12/a2  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_13" -alias x13/a3  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_14" -alias x14/a4  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_15" -alias x15/a5  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_16" -alias x16/a6  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_17" -alias x17/a7  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_18" -alias x18/s2  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_19" -alias x19/s3  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_20" -alias x20/s4  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_21" -alias x21/s5  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_22" -alias x22/s6  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_23" -alias x23/s7  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_24" -alias x24/s8  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_25" -alias x25/s9  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_26" -alias x26/s10 } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_27" -alias x27/s11 } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_28" -alias x28/t3  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_29" -alias x29/t4  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_30" -alias x30/t5  } 
lappend regfile_int {"ACoreChip.core.regfile_block.regfile.x_31" -alias x31/t6  } 
lappend regfile_int {"ACoreChip.core.csreg_block.csregs.mtvec" -alias mtvec  } 
lappend regfile_int {"ACoreChip.core.csreg_block.csregs.mepc" -alias mepc  } 


set regfile_float [ list ]
lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_0"  -alias f0/ft0   } 
lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_1"  -alias f1/ft1   } 
lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_2"  -alias f2/ft2   } 
lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_3"  -alias f3/ft3   } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_4"  -alias f4/ft4   } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_5"  -alias f5/ft5   } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_6"  -alias f6/ft6   } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_7"  -alias f7/ft7   } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_8"  -alias f8/fs0   } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_9"  -alias f9/fs1   } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_10" -alias f10/fa0  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_11" -alias f11/fa1  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_12" -alias f12/fa2  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_13" -alias f13/fa3  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_14" -alias f14/fa4  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_15" -alias f15/fa5  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_16" -alias f16/fa6  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_17" -alias f17/fa7  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_18" -alias f18/fs2  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_19" -alias f19/fs3  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_20" -alias f20/fs4  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_21" -alias f21/fs5  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_22" -alias f22/fs6  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_23" -alias f23/fs7  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_24" -alias f24/fs8  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_25" -alias f25/fs9  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_26" -alias f26/fs10 } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_27" -alias f27/fs11 } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_28" -alias f28/ft8  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_29" -alias f29/ft9  } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_30" -alias f30/ft10 } 
# lappend regfile_float {"ACoreChip.core.regfile_block.float_regfile.f_31" -alias f31/ft11 } 

set instruction_fetch [ list \
  {"ACoreChip.core.io_ifetch_req" -alias io_ifetch_req } \
  {"ACoreChip.core.io_ifetch_gnt" -alias io_ifetch_gnt } \
  {"ACoreChip.core.io_ifetch_raddr" -alias io_ifetch_raddr } \
  {"ACoreChip.core.io_ifetch_rdata" -alias io_ifetch_rdata } \
  {"ACoreChip.core.io_ifetch_rdata_valid" -alias io_ifetch_rdata_valid } \
]

set decoder [ list \
  {"ACoreChip.core.decoder_block.io_opcode" -alias opcode -radix Binary -tfile Opcode.txt} \
]

set lsu [ list ]
lappend lsu {"ACoreChip.lsu.io_core_req"}
lappend lsu {"ACoreChip.lsu.io_core_gnt"}
lappend lsu {"ACoreChip.lsu.io_core_info_ren"}
lappend lsu {"ACoreChip.lsu.io_core_info_wen"}
lappend lsu {"ACoreChip.lsu.io_core_info_op_width" -alias io_info_op_width -radix Decimal -tfile MemOpWidth.txt}
lappend lsu {"ACoreChip.lsu.io_core_info_addr"}
lappend lsu {"ACoreChip.lsu.io_core_info_wdata"}
lappend lsu {"ACoreChip.lsu.io_core_rdata_valid"}
lappend lsu {"ACoreChip.lsu.io_core_rdata_bits"}
lappend lsu {"ACoreChip.lsu.io_core_fault"}

set regfile_write [ list ]
lappend regfile_write {"ACoreChip.core.regfile_block.io_writeIn_data_alu_result"}
lappend regfile_write {"ACoreChip.core.regfile_block.io_writeIn_data_imm"}
lappend regfile_write {"ACoreChip.core.regfile_block.io_writeIn_data_mem_data"}
lappend regfile_write {"ACoreChip.core.regfile_block.io_writeIn_data_rd_waddr"}
lappend regfile_write {"ACoreChip.core.regfile_block.io_writeIn_control_wr_en"}
lappend regfile_write {"ACoreChip.core.regfile_block.io_writeIn_control_wr_in_select" \
  -alias io_writeIn_control_wr_in_select -radix Decimal -tfile RegFileSrc.txt}
lappend regfile_write {"ACoreChip.core.regfile_block.io_writeIn_control_wr_reg_select"}


# You can uncomment any of these if you dont want to see them
add_signal_list sys_signals Green {Core Signals}
add_signal_list pipeline Orange {Pipeline Signals}
add_signal_list PC Green {PC}
add_signal_list Trap Green {Trap Signals}
add_signal_list instruction_fetch Orange {Instruction Fetch}
add_signal_list IFA Orange {IFA-Stage}
add_signal_list IFD Yellow {IFD-Stage}
add_signal_list ID Orange {ID-Stage}
add_signal_list EX Yellow {EX-Stage}
add_signal_list MEMA Orange {MEMA-Stage}
add_signal_list MEMD Yellow {MEMD-Stage}
add_signal_list WB Orange {WB-Stage}
add_signal_list regfile_int Blue {Register File Integer}
add_signal_list regfile_float Yellow {Register File Float}
# add_signal_list regfile_write Orange {Register Write}
# add_signal_list decoder Violet {Decoder}
add_signal_list lsu Indigo {LSU}
# add_signal_list jtag Orange {JTAG Signals}


gtkwave::/Edit/UnHighlight_All
#gtkwave::/Time/Zoom/Zoom_Full
