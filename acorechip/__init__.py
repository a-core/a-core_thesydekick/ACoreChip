"""
========
ACoreChip
========

ACoreChip model template The System Development Kit
Used as a template for all TheSyDeKick Entities.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

For reference of the markup syntax
https://docutils.sourceforge.io/docs/user/rst/quickref.html

This text here is to remind you that documentation is iportant.
However, youu may find it out the even the documentation of this 
entity may be outdated and incomplete. Regardless of that, every day 
and in every way we are getting better and better :).

Initially written by Marko Kosunen, marko.kosunen@aalto.fi, 2017.


Role of section 'if __name__=="__main__"'
--------------------------------------------

This section is for self testing and interfacing of this class. The content of it is fully 
up to designer. You may use it for example to test the functionality of the class by calling it as
``pyhon3.6 __init__.py``

or you may define how it handles the arguments passed during the invocation. In this example it is used 
as a complete self test script for all the simulation models defined for the ACoreChip. 

"""

import os
import sys

thesdk_path = os.path.abspath(os.path.join(os.path.dirname(__file__),'../../thesdk'))
if not thesdk_path in sys.path:
    sys.path.append(thesdk_path)

from thesdk import *
from rtl import *
from rtl.connector import *
from spice import *

import numpy as np

class ACoreChip(rtl):
    """ACoreChip parameters and attributes

    Parameters
    ----------
    arg :
        If any arguments are defined, the first one should be the parent instance

    Attributes
    ----------
    proplist : list
        List of strings containing the names of attributes whose values are to be copied 
        from the parent

    Rs : float
        Sampling rate [Hz] of which the input values are assumed to change. Default: 100.0e6

    IOS : Bundle
        Members of this bundle are the IO's of the entity. See documentation of thsdk package.
        Default members defined as

        self.IOS.Members['A']=IO() # Pointer for input data
        self.IOS.Members['control_write']= IO() # Piter for control IO for rtl simulations

    """
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self, *arg):
        self.print_log(type='I', msg='Initializing %s' %(__name__)) 
        self.proplist = [ 'Rs' ];    # Properties that can be propagated from parent
        self.Rs =  100e6;            # Sampling frequency

        # io names
        self.parallel_inputs = []
        self.parallel_outputs = [
            'io_core_fault'
        ]
        self.tap_inputs = [
            'io_jtag_TCK',
            'io_jtag_TMS',
            'io_jtag_TDI',
            'io_jtag_TRSTn',
        ]
        self.tap_outputs = [
            'io_jtag_TDO_data',
            'io_jtag_TDO_driven'
        ]

        self.IOS=Bundle()
        self.IOS.Members['control_write']   = IO()  # simulation controls
        self.IOS.Members['in']              = IO()  # dut parallel inputs
        self.IOS.Members['out']             = IO()  # dut parallel outputs
        self.IOS.Members['jtag_tap_in']     = IO()  # jtag tap inputs
        self.IOS.Members['jtag_tap_out']    = IO()  # jtag tap outputs

        self.model = 'sv'   # Can be set externally, but is not propagated
        self.par = False    # By default, no parallel processing
        self.queue = []     # By default, no parallel processing

        # this copies the parameter values from the parent based on self.proplist
        if len(arg) >= 1:
            parent = arg[0]
            self.copy_propval(parent, self.proplist)
            self.parent = parent

        self.init()

    def init(self):
        """ Method to re-initialize the structure if the attribute values are changed after creation.

        """
        pass #Currently nohing to add

    def main(self):
        pass

    def run(self,*arg):
        ''' The default name of the method to be executed. This means: parameters and attributes 
        control what is executed if run method is executed. By this we aim to avoid the need of 
        documenting what is the execution method. It is always self.run. 
        '''
            
        if self.model in ['sv', 'icarus']:
            # serialize entity IO data for rtl simulation
            #_=rtl_iofile(self, name='in', dir='in', iotype='sample', ionames=self.parallel_inputs, datatype='sint')     # parallel inputs
            _=rtl_iofile(self, name='out', dir='out', iotype='sample', ionames=self.parallel_outputs, datatype='sint')      # parallel outputs
            _=rtl_iofile(self, name='jtag_tap_in', dir='in', iotype='sample', ionames=self.tap_inputs, datatype='sint')      # jtag tap inputs
            _=rtl_iofile(self, name='jtag_tap_out', dir='out', iotype='sample', ionames=self.tap_outputs, datatype='sint')   # jtag tap outputs

            # Verilog simulation options
            self.rtlparameters = { 'g_Rs': ('real', self.Rs) }
            self.run_rtl()
        else:
            self.print_log(type='F', msg="'%s' model currently not supported" % self.model)

    def define_io_conditions(self):
        '''This overloads the method called by run_rtl method. It defines the read/write conditions for the files
        '''
        # Programming inputs are read to verilog simulation after 'initdone' is set to 1 by controller
        # self.iofile_bundle.Members['jtag_programming_iface'].verilog_io_condition='initdone'
        # self.iofile_bundle.Members['programming_iface'].verilog_io_condition='initdone'
        # self.iofile_bundle.Members['prog_write_en'].verilog_io_condition='initdone'

        # Output is read to verilog simulation when all of the outputs are valid, 
        # and after 'initdone' is set to 1 by controller
        # self.iofile_bundle.Members['Z'].verilog_io_condition_append(cond='&& initdone')

    def add_simstop(self):
        '''Adds logic to Verilog testbench that stops simulation when program runs to end.
        '''
        self.custom_connectors = rtl_connector_bundle()
        self.custom_connectors.Members['simresult'] = rtl_connector(name='simresult', cls='wire')
        self.custom_connectors.Members['simstop'] = rtl_connector(name='simstop', cls='wire')
        self.custom_connectors.Members['mstopsim_stop'] = rtl_connector(name='acorechip.core.csreg_block.csregs.mstopsim[0]')
        self.custom_connectors.Members['mstopsim_result'] = rtl_connector(name='acorechip.core.csreg_block.csregs.mstopsim[1]')
        self.custom_connectors.Members['simstop'].connect = self.custom_connectors.Members['mstopsim_stop']
        self.custom_connectors.Members['simresult'].connect = self.custom_connectors.Members['mstopsim_result']
        self.assignment_matchlist = ['simstop', 'simresult']
        # Makes simulation stop when test program writes in mstopsim register
        stop_early_cmd = (
            "reg simstop_delayed;\n"
            "always @(posedge clock) begin\n"
            "    simstop_delayed <= simstop;\n"
            "end\n"
            "always @(simstop_delayed) begin\n"
            "    if (simstop_delayed == 1) begin\n"
            "        $finish;\n"
            "    end\n"
            "end"
        )
        self.rtlmisc = [stop_early_cmd]
        # Saves simresult to an iofile when mstopsim is 1
        self.IOS.Members['simresult'] = IO()
        simresult = rtl_iofile(self, name='simresult', dir='out', iotype='sample', ionames=['simresult'], datatype='int')
        simresult.verilog_io_condition = 'simstop == 1'

if __name__=='__main__':
    entity = ACoreChip()
    entity.print_log(type='F', msg='Self test not defined for the ACoreChip entity')
