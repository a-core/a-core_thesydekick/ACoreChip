import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *

class ACoreChiselCompiler(thesdk):
    """
    Class for compiling Chisel to Verilog for A-Core

    Parameters
    ----------
    hw_config_path : str
        Path to A-Core hw config
    jtag_config_path : str
        Path to JTAG config
    chisel_dir : str
        Location of A-Core chisel directory
    target_dir : str
        Desired target directory for Verilog files
    gen_cheaders : bool
        Whether to generate C header files
    cheaders_target_dir : str
        Path to desired target directory for C header files
    make_args : dict
        Arbitrary make parameters to propagate
    skip_clean : bool
        Skip make clean. Default False.
    skip_configure : bool
        Skip configure. Default False.
    skip_compile : bool
        Skip make ACoreChip. Default False.
    """
    def __init__(self, *arg, **kwargs):
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 

        self.hw_config_path      = kwargs.get("hw_config_path", None)
        self.jtag_config_path    = kwargs.get("jtag_config_path", None)
        self.chisel_dir          = kwargs.get("chisel_dir", None)
        self.target_dir          = kwargs.get("target_dir", None)
        self.gen_cheaders        = kwargs.get("gen_cheaders", None)
        self.cheaders_target_dir = kwargs.get("cheaders_target_dir", None)
        self.extra_args          = kwargs.get("make_args", [])
        self.skip_clean          = kwargs.get("skip_clean", False)
        self.skip_configure      = kwargs.get("skip_configure", False)
        self.skip_compile        = kwargs.get("skip_compile", False)

    def init(self):
        self.parse_options()
        self.compose_compile_cmd()
        self.compose_clean_prog_cmd()

    def parse_options(self):
        self.compile_args = {
            'core_config': self.hw_config_path,
            'jtag_config': self.jtag_config_path,
            'VERILOGPATH': self.target_dir,
            'gen_cheaders': self.gen_cheaders,
            'cheaders_target_dir': self.cheaders_target_dir
        }
        self.compile_args.update(self.extra_args)

    def configure(self):
        make_cmd = f"cd {self.chisel_dir} && ./configure"
        self.print_log(type='I', msg='Running configure.') 
        ret_val = os.system(make_cmd)
        if ret_val == 0:
            self.print_log(type='I', msg=f"Configure successful.")
        else:
            self.print_log(type='F', msg="Error while running configure. Ensure correct chisel path.")
    
    def clean(self):
        self.print_log(type='I', msg='Running make clean.') 
        ret_val = os.system(self.clean_cmd)
        if ret_val == 0:
            self.print_log(type='I', msg=f"Make clean successful.")
        else:
            self.print_log(type='F', msg="Error while cleaning. Ensure you run configure first.")

    def compose_clean_prog_cmd(self):
        arg_str = ""
        for arg in self.compile_args:
            arg_str += f"{arg}={self.compile_args[arg]} "
        self.clean_cmd = "make clean -C {} {}".format(self.chisel_dir, arg_str)

    def compose_compile_cmd(self):
        arg_str = ""
        for arg in self.compile_args:
            arg_str += f"{arg}={self.compile_args[arg]} "
        self.compile_cmd = "make ACoreChip -C {} {}".format(self.chisel_dir, arg_str)

    def compile(self):
        self.print_log(type='I', msg='Compiling ACoreChip.') 
        ret_val = os.system(self.compile_cmd)
        if ret_val == 0:
            self.print_log(type='I', msg=f"Compile successful. Sources at {self.target_dir}")
        else:
            self.print_log(type='F', msg="Error while compiling ACoreChip.")

    def check_none(self):
        for key, value in vars(self).items():
            if value is None:
                self.print_log(type='W', msg=f"{key} was None!")

    def run(self):
        self.check_none()
        if not self.skip_configure:
            self.configure()
        if not self.skip_clean:
            self.clean()
        if not self.skip_compile:
            self.compile()

class ACoreSWCompiler(thesdk):
    """
    Class for compiling sources for A-Core
    
    Parameters
    ----------
    test_config : dict
        A-Core test config
    hw_config : dict
        A-Core hw config
    acore_lib_path : str
        Path to ACore library
    acore_headers_path : str
        Path to ACore header files
    platform : str
        Target platform (sim, fpga)
    make_args : dict
        Arbitrary make parameters to propagate
    skip_clean : bool
        Skip make clean. Default False.
    skip_compile : bool
        Skip compile. Default False.
    target_dir : str
        Target directory for elf file

    Example
    -------
    ::

        compiler = ACoreSWCompiler(...)
        compiler.init()
        compiler.run()

    """

    def __init__(self, *arg, **kwargs):
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 

        self.test_config        = kwargs.get("test_config", None)
        self.hw_config          = kwargs.get("hw_config", None)
        self.acore_lib_path     = kwargs.get("acore_lib_path", None)
        self.acore_headers_path = kwargs.get("acore_headers_path", None)
        self.platform           = kwargs.get("platform", None)
        self.skip_clean         = kwargs.get("skip_clean", False)
        self.skip_compile       = kwargs.get("skip_compile", False)
        self.target_dir         = kwargs.get("target_dir", ".")

        self._bin = None

    @property
    def bin(self):
        """Path to compiled elf file."""
        if not self._bin:
            self.print_log(type='E', msg="Trying to acquire bin before compiled!")
        return self._bin

    def init(self):
        self.parse_options()
        self.compose_arg_str()
        self.compose_clean_prog_cmd()
        self.compose_compile_cmd()

    def parse_options(self):
        self.isa_string = self.hw_config["isa_string"] + "_zicsr"
        self.progmem_start = self.hw_config["pc_init"].replace("h", "0x")
        self.progmem_length = str(2**int(self.hw_config['progmem_depth']))
        self.datamem_start = "0x20000000"
        self.datamem_length = str(2**int(self.hw_config['datamem_depth'])) 
        
        self.extra_args  = self.test_config.get("make_args", [])
        self.source_path = self.test_config["test_program"]

        # Libraries
        self.libraries = []
        if self.test_config.get("libraries", []):
            self.libraries = self.test_config.get("libraries", [])

        self.compile_args = {
            'MARCH': self.isa_string,
            'MABI': 'ilp32f' if 'f' in self.isa_string else 'ilp32',
            'PLATFORM': self.platform,
            'A_CORE_LIB_PATH' : self.acore_lib_path,
            'A_CORE_HEADERS_PATH' : self.acore_headers_path,
            'DEFAULT_MAKEFILE_PATH' : self.acore_lib_path,
            'DEFAULT_LINKER_PATH' : self.acore_lib_path,
            'PROGMEM_START' : self.progmem_start,
            'PROGMEM_LENGTH' : self.progmem_length,
            'DATAMEM_START' : self.datamem_start,
            'DATAMEM_LENGTH' : self.datamem_length,
        }

        # Transfer library paths if libraries variable not empty
        if self.libraries:
            self.compile_args.update(
                {'LIBRARIES' : '\"' + ' '.join(map(str,self.libraries)) + '\"'}
            )

        # Update extra args
        self.compile_args.update(self.extra_args)

    # Note: this is the most important composer command!
    def compose_arg_str(self):
        self.arg_str = ""
        # Combine the compile_arguments into Make command argument string
        for arg in self.compile_args:
            self.arg_str += f"{arg}={self.compile_args[arg]} "

    def compose_clean_prog_cmd(self):
        self.clean_prog_cmd = "make clean -C {} {}".format(self.source_path, self.arg_str)

    def compose_compile_cmd(self):
        self.compile_cmd = "make -C {} {}".format(self.source_path, self.arg_str)
    
    def compose_compile_lib_cmd(self, lib_path):
        self.compile_lib_cmd = "make -C {} {}".format(lib_path, self.arg_str)
    
    def compose_clean_lib_cmd(self, lib_path):
        self.clean_lib_cmd = "make clean -C {} {}".format(lib_path, self.arg_str)

    def compile_lib(self, lib_path):
        # Compose the compile_lib_cmd
        self.compose_compile_lib_cmd(lib_path)

        # Call the compile_lib_cmd
        ret_val = os.system(self.compile_lib_cmd)
        if ret_val == 0:
            self.print_log(type='I', msg=f"Compiled {lib_path}.")
        else:
            self.print_log(type='F', msg=f"Error while compiling {lib_path}!")
        
    def compile_libraries(self):
        # Call compile lib 
        for lib in self.libraries:
            self.compile_lib(lib)

    def clean_lib(self, lib_path):
        # Compose the clean_lib_cmd
        self.compose_clean_lib_cmd(lib_path)

        # Call the command
        ret_val = os.system(self.clean_lib_cmd)
        if ret_val == 0:
            self.print_log(type='I', msg=f"Cleaned {lib_path}.")
        else:
            self.print_log(type='F', msg=f"Error while cleaning {lib_path}!")

    def clean_libraries(self):
        # Call compile lib 
        for lib in self.libraries:
            self.clean_lib(lib)

    def clean_program(self):
        self.print_log(type='I', msg=f'Cleaning software program.') 
        ret_val = os.system(self.clean_prog_cmd)
        if ret_val == 0:
            self._bin = os.path.join(self.source_path, f'{self.platform}.elf')
            self.print_log(type='I', msg=f"Source clean successful.")
        else:
            self.print_log(type='E', msg="Error while cleaning sources!")

    def clean(self):
        """Run ``make clean``."""
        self.clean_libraries()
        self.clean_program()

    def compile(self):
        """Run ``make`` to compile the software program."""
        # Compile libraries
        self.print_log(type='I', msg=f'Compiling software libraries.') 
        self.compile_libraries();
        # Compile and link the program
        self.print_log(type='I', msg=f'Compiling and linking software program.') 
        ret_val = os.system(self.compile_cmd)
        if ret_val == 0:
            bin_location = os.path.join(self.source_path, f'{self.platform}.elf')
            self._bin = os.path.join(self.target_dir, f'{self.platform}.elf')
            os.replace(bin_location, self._bin)
            self.print_log(type='I', msg=f"Compilation successful. Elf file at {self.bin}")
        else:
            self.print_log(type='F', msg="Error while compiling/linking program!")

    def check_none(self):
        """Check if any attribute is None."""
        for key, value in vars(self).items():
            if value is None and key != "_bin":
                self.print_log(type='W', msg=f"{key} was None!")

    def run(self):
        """Run compiler."""
        self.check_none()
        if not self.skip_clean:
            self.clean()
        if not self.skip_compile:
            self.compile()
        else:
            self._bin = os.path.join(self.source_path, f'{self.platform}.elf')

if __name__ == "__main__":
    ## Quick and dirty self test
    chisel_compiler = ACoreChiselCompiler(
        hw_config_path=f"{thesdk.HOME}/Entities/acorechip/hw_configs/core/rv32im.yml",
        jtag_config_path=f"{thesdk.HOME}/Entities/acorechip/hw_configs/jtag/jtag-config.yml",
        chisel_dir=f"{thesdk.HOME}/Entities/acorechip/chisel",
        target_dir=f"{thesdk.HOME}/Entities/acorechip/chisel/verilog",
        gen_cheaders=True,
        cheaders_target_dir=f"{thesdk.HOME}/Entities/acorechip/chisel/verilog/include",
    )
    chisel_compiler.init()
    chisel_compiler.run()
    test_config = {
        "test_program": f"{thesdk.HOME}/Entities/acoresoftware/sw/riscv-c-tests/src/hello_world",
        "libraries" : [f"{thesdk.HOME}/Entities/acoresoftware/lib/a-core-library/a-core-utils"],
        "make_args": [],
    }
    hw_config = {
        "isa_string": "rv32im",
        "pc_init": "h00001000",
        "progmem_depth": 16,
        "datamem_depth": 16,
    }
    compiler = ACoreSWCompiler(
        test_config=test_config,
        hw_config=hw_config,
        acore_lib_path=f"{thesdk.HOME}/Entities/acoresoftware/lib/a-core-library",
        acore_headers_path=f"{thesdk.HOME}/Entities/acorechip/chisel/verilog/include",
        platform="sim"
    )
    compiler.init()
    compiler.run()
