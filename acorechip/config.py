import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))
from thesdk import *
import yaml
import pprint

class ACoreTestConfig(thesdk):
    """
    Class for loading ACore test configurations.

    Parameters
    ----------
    config_path : str
        Absolute path to the test config 
    replace_list : dict
        Replacement list to replace an entry from the loaded yaml.
        Key indicates the entry to be replaced, and value the new value.
    """
    def __init__(self, **kwargs):
        self.print_log(type='I', msg='Inititalizing %s' %(__name__))
        self.config_path = kwargs.get("config_path", None)
        self.replace_list = kwargs.get("replace_list", {})
        self.init()

    def init(self):
        """Initialize."""
        self.constant_list = {
            "$ACORECHIP": os.path.join(thesdk.HOME, "Entities/acorechip"),
            "$ACORETESTS": os.path.join(thesdk.HOME, "Entities/acoretests"),
            "$ACORETESTBENCHES": os.path.join(thesdk.HOME, "Entities/acoretestbenches"),
            "$ACORESOFTWARE": os.path.join(thesdk.HOME, "Entities/acoresoftware"),
            "$ACORELIBRARY": os.path.join(thesdk.HOME, "Entities/acoresoftware/lib/a-core-library"),
            "$TEST_CONFIG_PATH": self.config_path
        }
        self.config = None


    def load_config_yaml(self, filepath):
        """
        Recursively load test configuration from yaml configuration files.

        Keys ending with ``_yaml`` contain paths to yaml configuration files.
        The contents of these files are loaded to keys without the suffix.

        For example, the contents of ``hw_config_yaml`` are loaded into dictionary
        corresponding to key ``hw_config``.

        If both ``sim_config_yaml`` and ``sim_config`` exists, the latter is used to override values
        provided in the former. This allows using ``sim_config_yaml`` as a template, while you can 
        still modify individual parameters on test-level.
        """

        loader = yaml.Loader
        test_config = None

        with open(filepath, 'r') as fd:
            test_config = yaml.load(fd, loader)
            self.replace_constants(test_config)

            # Load all different yamls
            yaml_list = [s for s in test_config.keys() if s.endswith("_yaml")]
            for yaml_name in yaml_list:
                new_name = yaml_name[:-len("_yaml")]
                if yaml_name in self.replace_list.keys():
                    new_config = self.load_config_yaml(self.replace_list[yaml_name])
                    self.print_log(type='I', msg=f'Replacing {yaml_name} with {self.replace_list[yaml_name]}')
                else:
                    new_config = self.load_config_yaml(test_config[yaml_name])
                if new_name in test_config:
                    new_config.update(test_config[new_name])
                test_config[new_name] = new_config


        return test_config

    def replace_constants(self, config):
        """
        Replace constant values in a config file.
        """
        for config_key, config_value in config.items():
            if isinstance(config_value, dict):
                self.replace_constants(config_value)
            elif isinstance(config_value, list):
                for i in range(len(config_value)):
                    config_value[i] = self.replace_constant_value(config_value[i])
            else:
                config[config_key] = self.replace_constant_value(config_value)

    def replace_constant_value(self, value):
        for constant_key, constant_value in self.constant_list.items():
            if constant_key in str(value):
                value = str(value).replace(constant_key, constant_value)
        return value

    def print_config(self):
        """Print the config that was loaded."""
        msg = \
            "Loaded the following config:\n" + \
            yaml.dump(self.config, default_flow_style=False)
        self.print_log(type='I', msg=msg)

    def run(self):
        """Run config loader."""
        self.config = self.load_config_yaml(self.config_path)
        self.print_config()

