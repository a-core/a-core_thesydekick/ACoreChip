# Written by Marko kosunen, Marko.kosunen@aalto.fi 20190530
# The right way to do the unit controls is to write a controller class here
import os

import numpy as np
from thesdk import *
from rtl import *
from rtl.module import *
from jtag.driver import jtag_driver

class controller(rtl):
    """A-Core controller.
    
    This class defines simulation-related things, such as the input IOs,
    reset sequence, simulation stop."""
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self, dut, **kwargs): 
        '''Parameters
           ----------
              **kwargs :
                 file: str
                    File path pointing to the Verilog file for the module.
                    By default, it is simdut (that file is generated under simulations/ folder for standard simulations).
                    For example, if you want to avoid generating Chisel separately for every test,
                    you can generate it to a supported location (e.g. sv/ folder) and provide it here.
                    This is used in TheSDK riscv-tests.

        '''
        self.Rs = dut.Rs; # Sampling frequency
        self.step=int(1/(self.Rs*self.rtl_timescale_num)) # Time increment for control
        self.time=0

        self.IOS=Bundle()
        self.IOS.Members['control_write']= IO()        #We use this for writing
        _=rtl_iofile(self, name='control_write', dir='in', iotype='event', ionames=['initdone', 'reset'])
        #Permanent pointer assignment to write io
        self.IOS.Members['control_write'].Data=self.iofile_bundle.Members['control_write']
 
        #self.IOS.Members['control_read']= IO()        #We use this for reading
        #_=rtl_iofile(self, name='control_read', dir='out', iotype='event', datatype='int',
        #        ionames=['initdone', 'reset'])        

        self.model='sv';             #can be set externally, but is not propagated
        self.par= False              #By default, no parallel processing
        self.queue= []               #By default, no parallel processing

        # We know where the rtl file is. 
        # Let's read in the file to have IOs defined
        self.vlogext = '.v'
        
        file = kwargs.get('file', dut.simdut)
        self.dut=verilog_module(file=file)

        # Define the signal connectors associated with this 
        # controller
        # These are signals of tb driving several targets
        # Not present in DUT
        self.connectors=rtl_connector_bundle()

        #These are signals not in dut
        self.newsigs_write=[
                 'initdone',
                 'simdone',
                 'progdone',
                ]

        # Selected signals controlled with this file with init values
        # These are tuples defining name init value pair
        self.signallist_write=[
            ('reset', 1),
            ('initdone',0),
            ('simdone',0),
            ('progdone',0),
            ('io_gpi', 0x55),
            #('io_core_en',0),
        ]

        #These are signals not in dut
        self.newsigs_read=[
                ]
        self.signallist_read=[
        ]
        self.init()

    def init(self):
        self.rtlparameters = { 'g_Rs': ('real', self.Rs) }
        # This gets interesting
        # IO is a file data stucture
        self.define_control()

    def reset_control_sequence(self):
        f=self.iofile_bundle.Members['control_write']
        self.time=0
        f.Data= np.array([])
        f.set_control_data(init=0) # Initialize to zeros at time 0
        self.assign_io()

    # First we start to control Verilog simulations with 
    # This controller. I.e we pass the IOfile definition
    def step_time(self,**kwargs):
        self.time+=kwargs.get('step',self.step)

    def define_control(self):
        # This is a bit complex way of passing the data,
        # But eventually we pass only the data , not the file
        # Definition. File should be created in the testbench
        scansigs_write=[]
        for name, val in self.signallist_write:
            # We manipulate connectors as rtl_iofile operate on those
            if name in self.newsigs_write:
                self.connectors.new(name=name, cls='reg')
            else:
                self.connectors.Members[name]=self.dut.io_signals.Members[name]
                self.connectors.Members[name].init=''
            scansigs_write.append(name) 

        f=self.iofile_bundle.Members['control_write']
        #define connectors controlled by this file in order of the list provided 
        f.verilog_connectors=self.connectors.list(names=scansigs_write)
        f.set_control_data(init=0) # Initialize to zeros at time 0

    #Methods to reset and to start datafeed
    def reset(self):
        #start defining the file
        f=self.iofile_bundle.Members['control_write']
        for name,value in self.signallist_write:
            f.set_control_data(time=self.time,name=name,val=value)

        # After awhile, switch off reset 
        self.step_time(step=15*self.step)

        for name in [ 'reset', ]:
            f.set_control_data(time=self.time,name=name,val=0)

    def start_datafeed(self):
        f=self.iofile_bundle.Members['control_write']
        for name in [ 'initdone', ]:
            f.set_control_data(time=self.time,name=name,val=1)
        self.step_time()

    def set_simdone(self):
        f=self.iofile_bundle.Members['control_write']
        for name in [ 'simdone', ]:
            f.set_control_data(time=self.time, name=name, val=1)
        self.step_time()

    def set_progdone(self):
        """Set flag signal for JTAG programming done"""
        f=self.iofile_bundle.Members['control_write']
        for name in [ 'progdone' ]:
            f.set_control_data(time=self.time, name=name, val=1)
        self.step_time()


class jtag_controller(thesdk):
    """Class for handling JTAG control transactions, such as
    setting core enable high, or writing to arbitrary test data registers.
    """
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self, driver, config): 
        self.proplist = [ 'Rs' ];           # Properties that can be propagated from parent
        self.Rs = 100e6;                    # Sampling frequency

        assert isinstance(driver, jtag_driver), "driver must be an instance of jtag_driver"
        self.driver = driver
        self.config = config

    def set_tdr(self, name, value):
        """Set a JTAG test data register to some value.
        
        Parameters
        ==========

        name : str
            Name of the TDR as in the jtag_config
        value : int
            Value to be set

        """
        # look up the tdr by name
        tdr_config = None
        for tdr in self.config['tdrs']:
            if tdr['name'] == name:
                tdr_config = tdr
                break
        else:
            raise ValueError(f"TDR not found in the JTAG config: {name}")

        # construct ir string for the tdr
        fmt_ir = "0%sb" % self.config['ir_width']
        ir_str = format(tdr_config['ir'], fmt_ir)

        # construct tdr data string
        # with zero padding
        fmt_tdr = "0%sb" % tdr_config['width']
        tdr_str = format(value, fmt_tdr)

        # shift value to tdr
        self.driver.shift_to_chain(ir_str, tdr_str)

    def set_core_en(self, value):
        """Set core_en TDR."""
        self.set_tdr("core_en", value)

