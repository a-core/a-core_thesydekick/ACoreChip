API
===

.. autoclass:: acorechip.ACoreChip
    :members:
    :undoc-members:

.. autoclass:: acorechip.acore_compilers.ACoreChiselCompiler
    :members:
    :undoc-members:

.. autoclass:: acorechip.acore_compilers.ACoreSWCompiler
    :members:
    :undoc-members:

.. autoclass:: acorechip.config.ACoreTestConfig
    :members:
    :undoc-members:

.. autoclass:: acorechip.controller.controller
    :members:
    :undoc-members:

.. autoclass:: acorechip.controller.jtag_controller
    :members:
    :undoc-members: